const log = console.log;

const Utils = {
  feetToMeter: ft => ft * 0.3048,
  knotToKmh: kt => kt * 1.852,
  degreesToRadians: degrees => (degrees * Math.PI) / 180,
  distanceInKmBetweenEarthCoordinates: (
    { lat: lat1, lng: lng1 },
    { lat: lat2, lng: lng2 }
  ) => {
    const earthRadiusKm = 6371;

    const dLat = Utils.degreesToRadians(lat2 - lat1);
    const dLon = Utils.degreesToRadians(lng2 - lng1);

    lat1 = Utils.degreesToRadians(lat1);
    lat2 = Utils.degreesToRadians(lat2);

    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return earthRadiusKm * c;
  }
};

const JsonDecoder = {
  decodeFlight(jsonArr) {
    return {
      num: jsonArr[13],
      coords: { lat: jsonArr[1], lng: jsonArr[2] },
      bearing: jsonArr[3],
      altitude: Utils.feetToMeter(jsonArr[4]),
      speed: Utils.knotToKmh(jsonArr[5]),
      originAirportCode: jsonArr[11],
      destinationAirportCode: jsonArr[12]
    };
  }
};

const Api = (function() {
  const ENDPOINT_URL = "https://data-live.flightradar24.com";

  return {
    async getFlightsWithinBound([north, south, west, east]) {
      const response = await fetch(
        `${ENDPOINT_URL}/zones/fcgi/feed.js?bounds=${north},${south},${west},${east}`,
        {
          mode: "cors",
          redirect: "follow",
          referrer: "no-referrer",
          referrerPolicy: "no-referrer"
        }
      );
      const jsonBody = await response.json();

      return Object.keys(jsonBody)
        .filter(
          k =>
            !["full_count", "version"].includes(k) && Array.isArray(jsonBody[k])
        )
        .map(k => JsonDecoder.decodeFlight(jsonBody[k]));
    }
  };
})();

/**
 * Virtual DOM.
 */
const View = {
  numTag(strings, ...exps) {
    let val = strings[0];
    exps.forEach(
      (v, i) =>
        (val +=
          v.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ") + strings[i + 1])
    );

    return ["span", [], [val]];
  },
  viewHeader() {
    return [
      "tr",
      [],
      [
        ["th", [], ["Координаты"]],
        ["th", [], ["Скорость, км/ч"]],
        ["th", [], ["Курс, °"]],
        ["th", [], ["Высота полета, м"]],
        ["th", [], ["Аэропорт"]],
        ["th", [], ["Номер рейса"]]
      ]
    ];
  },
  viewRow(fl) {
    return [
      "tr",
      [],
      [
        [
          "td",
          [],
          [`${fl.coords.lat.toFixed(2)}, ${fl.coords.lng.toFixed(2)}`]
        ],
        ["td", [], [View.numTag`${Math.round(fl.speed)}`]],
        ["td", [], [View.numTag`${fl.bearing}`]],
        ["td", [], [View.numTag`${Math.round(fl.altitude)}`]],
        [
          "td",
          [],
          [
            `${fl.originAirportCode || "???"} 🡒 ${fl.destinationAirportCode ||
              "???"}`
          ]
        ],
        ["td", [], [`${fl.num || "—"}`]]
      ]
    ];
  },
  viewTable(flights) {
    return [
      "section",
      [],
      [
        ["h1", [], ["Список рейсов вблизи аэропорта Домодедово"]],
        [
          "table",
          [],
          [
            ["thead", [], [View.viewHeader()]],
            ["tbody", [], flights.map(View.viewRow)]
          ]
        ]
      ]
    ];
  }
};

/**
 * Renderer with simple logic.
 * Create dom while traversing list and replace root first child with new dom tree.
 */
const Renderer = {
  /**
   * Create DOM tree.
   * @param {VirtualNode} view 
   */
  createDom(view) {
    if (typeof view === "string") {
      return document.createTextNode(view);
    }
    const [name, _attrs, childrens] = view;

    const node = document.createElement(name);
    childrens &&
      childrens.forEach(ch => node.appendChild(Renderer.createDom(ch)));

    return node;
  },
  /**
   * Create DOM tree from virtual and replace `domRoot` child.
   * @param {HTMLElement} domRoot 
   * @param {VirtualNode} view Virtual dom in format `[nodeName, [], [VirtualNode]]`
   */
  render(domRoot, view) {
    domRoot.firstElementChild && domRoot.firstElementChild.remove();
    domRoot.appendChild(Renderer.createDom(view));
  }
};

const main = (config = {}) => {
  const STATES = Object.freeze({
    IDLE: 1,
    RUNNING: 2
  });

  const defaultConfig = {
    update_interval: 5000, // ms
    targetAirportCoords: { lat: 55.410307, lng: 37.902451 } // Domodedovo airport
  };

  const { update_interval, targetAirportCoords } = {
    ...defaultConfig,
    ...config
  };
  const root = document.getElementById("app");
  let timerId;
  let state = STATES.IDLE;

  const runUpdate = async () => {
    if ("onLine" in window.navigator && !window.navigator.onLine) {
      clearTimeout(timerId);
      timerId = null;
      return;
    }

    try {
      state = STATES.RUNNING;
      const flights = await Api.getFlightsWithinBound([
        56.84,
        55.27,
        33.48,
        41.48
      ]);
      const sortedFlights = flights
        .map(fl => [
          Utils.distanceInKmBetweenEarthCoordinates(
            targetAirportCoords,
            fl.coords
          ),
          fl
        ])
        .sort((a, b) => a[0] - b[0])
        .map(t => t[1]);

      Renderer.render(root, View.viewTable(sortedFlights));
    } catch (err) {
      // Renderer.render(root, View.viewError('Ошибка при выполнении запроса'));
      log(`Error in update loop: ${err}`);
    } finally {
      state = STATES.IDLE;
      timerId = setTimeout(runUpdate, update_interval);
    }
  };

  runUpdate();

  window.addEventListener("online", () => {
    if (state == STATES.IDLE && !timerId) {
      runUpdate();
    }
  });
};

main();
